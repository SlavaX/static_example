#include <iostream>

class Singleton
{
public:
    static Singleton &inst()
    {
        std::cout << "static Singleton &inst()" << std::endl;
        static Singleton localVar("localVar");
        return localVar;
    }
private:
    Singleton(std::string str): str(str)
    {
        std::cout << "constructor: " << str << std::endl;
    }
    ~Singleton()
    {
        std::cout << "destructor: " << str << std::endl;
    }
    static Singleton classMember;
    std::string str;
};
Singleton Singleton::classMember("classMember");

int main()
{
    std::cout << "main begin" << std::endl;
    Singleton::inst();
    std::cout << "main end" << std::endl;
    return 0;
}
